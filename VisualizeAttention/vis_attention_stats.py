import torch
from sklearn.metrics import *
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from main_script import *
idx_2_label = {0:'Other',
            1: 'Open Door 1',
            2: 'Open Door 2',
            3: 'Close Door 1',
            4: 'Close Door 2',
            5: 'Open Fridge',
            6: 'Close Fridge',
            7: 'Open Dishwasher',
            8: 'Close Dishwasher',
            9: 'Open Drawer 1',
            10: 'Close Drawer 1',
            11: 'Open Drawer 2',
            12: 'Close Drawer 2',

            13: 'Open Drawer 3',
            14: 'Close Drawer 3',
            15: 'Clean Table',
            16: 'Drink from Cup',
            17: 'Toggle Switch' }
# matplotlib.rcParams.update({'font.size': 22})
label_2_idx = {v:k for k,v in idx_2_label.items()}

pred_dir = '/coc/pcba1/vmurahari3/deepconvlstmattention/VisualizeAttention/preds.csv' # location of pred filess
attn_dir = '/coc/pcba1/vmurahari3/deepconvlstmattention/VisualizeAttention/attn.csv'
# load model
model = RCNN(input_size=NB_SENSOR_CHANNELS * NUM_FILTERS, hidden_size=128,
             num_layers=2, is_bidirectional=False, dropout=0.79, attention_dropout=0.74)
if torch.cuda.is_available():
    model = model.cuda()
saved_state = torch.load("0.7065222851937571..0.74..0.79..0.9611")
model.load_state_dict(saved_state)
evaluate(model, visualize_attention=True,
         out_dir=attn_dir, pred_dir=pred_dir)
# evaluate(model)
# read the pred file
df = pd.read_csv(pred_dir)
preds = df.as_matrix()
attn_df = pd.read_csv(attn_dir,skiprows=1, header=None).as_matrix()
# we need to find out class-wise sample by sample accuracy
plt.xlabel("Hidden States")
plt.ylabel("Attention Weights")
ind = np.arange(7)
for cls in idx_2_label:
     if cls != 0:
         preds_class = np.zeros_like(preds)
         preds_class = preds_class * -1
         preds_class[preds == cls] = 1
         print(" " + idx_2_label[cls] + " " + str(f1_score(y_pred=preds_class[:,0], y_true=preds_class[:,1])))
         #  plotting attention weights

         cls_indices,_ = np.where(preds == cls)
         attn_cls = attn_df[cls_indices, 1:-2]
         attn_weights_cls = np.median(attn_cls,axis=0)
         plt.bar(x=ind, height=attn_weights_cls, label = idx_2_label[cls])
         ind = ind + 0.1
         # , label = idx_2_label[cls]

plt.legend(loc='best')
plt.show()




